[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/fitchn%2Foqtant_on_binder/main)  
^^^ Click this button to open this project on Binder and explore Oqtant without installing anything locally!

# Oqtant on Binder

This is an example repository for how to get Oqtant jupyter materials operational on [Binder](https://mybinder.org). If you want to engage others and get them using Oqtant, but don't want them to have to download and/or install anything on their local machine, you can use this repository as a starting point / template for developing your own materials. This usage model is ideal for coursework, tutorials, and lectures.  

Note: Remotely hosted jupyter servers (and jupyter lab in general) are not typically compatible with Oqtant's automatic user authentication, which uses a popup widget.  As such, the examples included in this repository use the "manual" authentication method that involves copying an access token from the Oqtant web app and using that token in the jupyter notebooks.

Note: Binder is amazingly flexible, but also can be somewhat slow, especially for initial setup or when you push repo changes and it needs to rebuild and push the Docker container.

Note: This repository uses pre-commit to, among other things, ensure you don't commit personal access
tokens.  This is highly recommended, but is not absolutely required.

Note: The instructions below assume you already have python >3.10 installed and are working on a Linux-based machine.  Please alter the command line steps as necessary for your specific operating system. Also, the exact commands, e.g. python / python3 / py or pip / pip3 may depend on the details of your python installation.

## Repository setup ##

Clone the repository and navigate to the newly created folder

```shell
git clone git@gitlab.com:fitchn/oqtant_on_binder.git
cd oqtant_on_binder
```

### Create a virtual environment, activate it, and install dependencies ###

Here, I assume you have your project dependencies listed in a *requirements.txt* file, as done in this example repository.

```shell
python -m venv .venv
source .venv/bin/activate
pip install -r .binder/requirements.txt
```

### Configure pre-commit ###

```shell
pre-commit install
pre-commit install --hook-type pre-push
```

## Testing locally and developing your own materials ##

You can test out your own notebooks using a local jupyter server. Here's how you do that.
Keep in mind that Binder uses jupyter lab by default.

```shell
jupyter lab
```

or, if you want to use classical jupyter notebooks instead

```shell
jupyter notebook
```

To get started, you can check out the jupyter materials in this repository's *examples/* and *events/* directories. At the time of this writing, the examples are lightly altered versions of Oqtant's official documentation notebooks as they stand in June 2024. The only real change is the exclusive use of the "manual" user authentication method, which is necessary when working on Binder.

In the end, you'll want to set up your own repository with your desired notebooks/content and, at a minimum, a *requirements.txt* file that lists your project dependencies.  You can place this file either at the top level of the project or within a *.binder* folder as I've done here.  With the latter option, you can also include *runtime* and *postBuild* instructions for Binder's Docker container creation and setup.

Note: Binder is also compatible with using a Conda *environment.yml* file to specify the details of the created environment. 

## 'Publish' on Binder ##

Go to [Binder](https://mybinder.org) and follow the instructions for connecting
your repository and making it publically accessible.  During setup, you will have access to 
a public link that you can share with others for running your materials. 
